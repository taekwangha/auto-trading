from flask import Flask
from flask_restful import Api
from api.Coin import Coin
from api.Trade import Trade

app = Flask(__name__)
api = Api(app)

api.add_resource(Coin, '/api/coin/<coin_type>/price_info')
api.add_resource(Trade, '/api/trade')

if __name__ == '__main__':
    app.run(debug=True)
