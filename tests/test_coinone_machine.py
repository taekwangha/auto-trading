"""
Stocker class TestCase
"""
import unittest
from machine.coinone_machine import CoinOneMachine
import inspect
import configparser
import inspect

class CoinOneMachineTestCase(unittest.TestCase):
    """
    Test CoinOneMachine
    """
    def setUp(self):
        config = configparser.ConfigParser()
        config.read('conf/config.ini')
        access_token = config['COINONE']['access_token']
        secret_key = config['COINONE']['secret_key']
        username = "hyunny82@gmail.com"
        self.coinone_machine = CoinOneMachine(access_token=access_token,
                                            secret_key=secret_key,
                                            username=username)

    def testSetToken(self):
        print(inspect.stack()[0][3])
        expire, access_token, refresh_token = self.coinone_machine.set_token()
        assert access_token
        print(access_token)

    def test_get_wallet_status(self):
        print(inspect.stack()[0][3])
        result = self.coinone_machine.get_wallet_status()
        assert result
        print(result)
    
    def test_get_ticker(self):
        print(inspect.stack()[0][3])
        result = self.coinone_machine.get_ticker(coin_type="xrp")
        assert result
        print(result)

    """
    def test_buy_coin_order(self):
        print(inspect.stack()[0][3])
        result = self.coinone_machine.buy_coin_order(coin_type="xrp",
                                                     price="230",
                                                     qty="1",
                                                     order_type="limit")
        assert result
        print(result)
    def test_sell_coin_order(self):
        print(inspect.stack()[0][3])
        result = self.coinone_machine.sell_coin_order(coin_type="xrp",
                                                     price="230",
                                                     qty="1",
                                                     order_type="limit")
        assert result
        print(result)
    def test_cancel_coin_order(self):
        print(inspect.stack()[0][3])
        result = self.coinone_machine.cancel_coin_order(coin_type="xrp",
                                                        price="230",
                                                        qty="1",
                                                        order_type="buy",
                                                        order_id="76289afa-2635-4c6e-8ce4-38d67a5c1152")
        assert result
        print(result)
        """
    
    def test_get_list_my_orders(self):
        print(inspect.stack()[0][3])
        result = self.coinone_machine.get_list_my_orders(coin_type="xrp")
        assert result
        print(result)

    def test_get_my_order_status(self):
        print(inspect.stack()[0][3])
        result = self.coinone_machine.get_my_order_status(coin_type="xrp", order_id="60066de5-bd9f-4fcf-8381-f623d4e88101")
        assert result
        print(result)

    def tearDown(self):
        pass
