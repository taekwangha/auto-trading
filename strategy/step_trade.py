from strategy.base_strategy import Strategy
from machine.korbit_machine import KorbitMachine
from machine.coinone_machine import CoinOneMachine
from db.mongodb.mongodb_handler import MongoDbHandler
from pusher.slack import PushSlack
import configparser, datetime, traceback, sys
from logger import get_logger
import redis

logger = get_logger("step_trade")

class StepTrade(Strategy):
     
    def __init__(self, machine=None, db_handler=None, strategy=None, coin_type=None, pusher=None):
        if machine is None or db_handler is None or coin_type is None or strategy is None:
            raise Exception("Need to machine, db, coin type, strategy")
        if isinstance(machine, KorbitMachine):
            logger.info("Korbit machine")
            self.coin_type=coin_type+"_krw"
        elif isinstance(machine, CoinOneMachine):
            logger.info("CoinOne machine")
        self.machine = machine
        self.pusher = pusher
        self.db_handler = db_handler
        result = self.db_handler.find_item({"name":strategy},"trader","trade_strategy")
        self.params = result[0] 
        #prevent token call
        if self.params["is_active"]=="inactive":
            logger.info("inactive")
            return 
        self.redis = redis.StrictRedis(host='localhost', port=6379, db=0)
        self.token = self.redis.get(str(self.machine)+self.machine.get_username())

        if self.token == None:
            logger.info("set new token")
            saved_refresh_token = self.redis.get(str(self.machine)+self.machine.get_username()+"refresh")
            if saved_refresh_token is None:
                expire, token, refresh = self.machine.set_token(grant_type="password")
            else:
                self.machine.refresh_token = saved_refresh_token.decode("utf-8")
                expire, token, refresh = self.machine.set_token(grant_type="refresh_token")
            self.redis.set(str(self.machine)+self.machine.get_username(), token, expire)
            self.redis.set(str(self.machine)+self.machine.get_username()+"refresh", refresh, 3500)
            self.token = token
        else:
            self.token = self.token.decode("utf-8")
            self.machine.access_token = self.token

        logger.info(self.token)
        logger.info(self.coin_type)
        last = self.machine.get_ticker(self.coin_type)
        self.last_val=int(last["last"])

    def scenario(self):
        now = datetime.datetime.now()
        ten_min_ago = now - datetime.timedelta(minutes=10)
        ten_min_ago_timestamp = int(ten_min_ago.timestamp())
        five_min_ago = now - datetime.timedelta(minutes=5)
        five_min_ago_timestamp = int(five_min_ago.timestamp())
        pipeline = [{"$match":{"timestamp":{"$gt":ten_min_ago_timestamp,"$lte":five_min_ago_timestamp}, "coin":self.coin_type}},
                    {"$group":{"_id":"$coin",
                               "min_val":{"$min":"$price"},
                               "max_val":{"$max":"$price"},
                               "sum_val":{"$sum":"$amount"}
                    }}]

        query_result = self.db_handler.aggregate(pipeline,"coiner","price_info")
        for item in query_result:
            ten_max_val = int(item["max_val"])
            ten_min_val = int(item["min_val"])
            ten_sum_val = int(item["sum_val"])/5
            ten_gap = ten_max_val - ten_min_val 
        logger.info('last:'+str(self.last_val))
        logger.info('t_max:'+str(ten_max_val))
        logger.info('t_min:'+str(ten_min_val))
        logger.info('5_sum:'+str(ten_sum_val))
        logger.info('t_gap:'+str(ten_gap))

        one_day_ago = now - datetime.timedelta(minutes=1500)
        one_day_ago_timestamp = int(one_day_ago.timestamp())
        pipeline_amount = [{"$match":{"timestamp":{"$gt":one_day_ago_timestamp,"$lte":five_min_ago_timestamp}, "coin":self.coin_type}},
                           {"$group":{"_id":"$coin",
                                      "min_val":{"$min":"$price"},
                                      "max_val":{"$max":"$price"},
                                      "sum_val":{"$sum":"$amount"}
                    }}]

        day_query_result = self.db_handler.aggregate(pipeline_amount)

        for item in day_query_result:
            day_max_val = int(item["max_val"])
            day_min_val = int(item["min_val"])
            day_sum_val = int(item["sum_val"])
            day_sum_avg_val = int(item["sum_val"])/1440
            day_gap = day_max_val - day_min_val 

        logger.info('last:'+str(self.last_val))
        logger.info('d_max:'+str(day_max_val))
        logger.info('d_min:'+str(day_min_val))
        logger.info('d_sum:'+str(day_sum_val))
        logger.info('d_avg:'+str(day_sum_avg_val))
        logger.info('d_gap:'+str(day_gap))

        pipeline_5m = [{"$match":{"timestamp":{"$gt":five_min_ago_timestamp}, "coin":self.coin_type}},
                           {"$group":{"_id":"$coin",
                                      "min_val":{"$min":"$price"},
                                      "max_val":{"$max":"$price"},
                                      "sum_val":{"$sum":"$amount"}
                    }}]

        five_min_result = self.db_handler.aggregate(pipeline_5m)

        for item in five_min_result:
            five_max_val = int(item["max_val"])
            five_min_val = int(item["min_val"])
            five_sum_val = int(item["sum_val"])
            five_sum_avg_val = int(item["sum_val"])/5
            five_gap = five_max_val - five_min_val 

        logger.info('last:'+str(self.last_val))
        logger.info('5_max:'+str(five_max_val))
        logger.info('5_min:'+str(five_min_val))
        logger.info('5_sum:'+str(five_sum_val))
        logger.info('5_avg:'+str(five_sum_avg_val))
        logger.info('5_gap:'+str(five_gap))

        "Buy logic part"
        send_msg ={"coin":self.coin_type, "last":self.last_val, "d_max":day_max_val, "d_min":day_min_val,
                    "d_avg":day_sum_avg_val,"d_gap":day_gap, "t_max":ten_max_val, "h_min":ten_min_val,
                    "t_sum":ten_sum_val, "h_gap:":ten_gap, "5_min":five_min_val, "5_max":five_max_val, "5_gap":five_gap}
        "if down stream dont try buy"
        if five_min_val < ten_min_val and ten_sum_val > day_sum_avg_val: #down stream
            self.pusher.send_message("#push", "down stream:"+str(send_msg))
            return
        "if up stream try buy"
        if five_max_val > ten_max_val and five_sum_val > day_sum_avg_val  : #up stream
            logger.info("upstream")
            #self.pusher.send_message("#push", "up stream"+str(send_msg))
        logger.info("Normal stream")
        logger.info("buy_price:"+str(self.last_val))
        my_orders = self.db_handler.find_item({"coin":self.coin_type, "$or":[{"status":"BUY_ORDERED"},{"status":"SELL_ORDERED"},{"status":"BUY_COMPLETED"}],
                                                                    "buy":{"$gte": self.last_val-int(self.params["step_value"]),
                                                                            "$lte": self.last_val+int(self.params["step_value"])}},"trader","trade_status")
        "Check duplicate price bewteen instep_value"
        if my_orders.count() > 0:
            logger.info("Exists order in same price")
            for order in my_orders:
                logger.info("order:"+str(order))
        else:
            logger.info("Buy Order")
            self.item={"buy":str(self.last_val), "buy_amount":self.params["buy_amount"], "coin":self.coin_type}
            self.item["desired_value"] = int(self.last_val+int(self.params["target_profit"])/float(self.params["buy_amount"]))
            logger.info(self.item)
            wallet = self.machine.get_wallet_status()
            if int(wallet["krw"]["avail"]) > int(self.last_val*float(self.params["buy_amount"])):
                self.order_buy_transaction(machine=self.machine,
                                           db_handler=self.db_handler,
                                           coin_type=self.coin_type,
                                           item=self.item)
                #self.pusher.send_message("#push", "buy_ordered"+str(self.item))
                self.check_buy_ordered()
            else:
                logger.info("krw is short")

    def check_buy_ordered(self):
        #Check buy ordered
        buy_orders = self.db_handler.find_item({"coin":self.coin_type,"status":"BUY_ORDERED"}, "trader", "trade_status")

        for item in buy_orders:
            logger.info(item)
            order_result = self.machine.get_my_order_status(self.coin_type, order_id=item["buy_order_id"])
            logger.info(order_result)
            if len(order_result)>0 and order_result[0]["status"]=="filled" and order_result[0]["price"] == str(item["buy"]):
                order_result_dict = order_result[0]
                real_buy_amount = float(order_result_dict["filled_amount"])-float(order_result_dict["fee"])
                real_buy_value = float(order_result_dict["avg_price"])
                completed_time = int(order_result_dict["last_filled_at"]/1000)
                fee = float(order_result_dict["fee"])
                if order_result_dict["side"] == "bid":
                    self.update_trade_status(db_handler=self.db_handler,
                                             buy={"_id":item["_id"]},
                                             value={"status":"BUY_COMPLETED",
                                              "real_buy_amount":real_buy_amount,
                                              "buy_completed_time":completed_time,
                                              "real_buy_value":real_buy_value,
                                              "buy_fee":fee,
                                              "error":"success"})
                    #self.pusher.send_message("#push", "buy_completed:"+str(item))
            elif len(order_result)>0 and order_result[0]["status"] =="unfilled" and order_result[0]["price"] == str(item["buy"]):
                if int(item["buy"])+int(self.params["step_value"]) <= self.last_val:
                    logger.info("CancelOrder")
                    logger.info(item)
                    #Cancel Order
                    try:
                        self.order_cancel_transaction(machine=self.machine, db_handler=self.db_handler, coin_type=self.coin_type, item=item)
                    except:
                        error = traceback.format_exc()
                        logger.info(error)
                        self.update_trade_status(db_handler=self.db_handler,
                                                    buy={"_id":item["_id"]},
                                                    value={"error": "failed"})
                        self.pusher.send_message("#push", "err cancle:"+str(item))
            elif len(order_result) == 0:
                self.update_trade_status(db_handler=self.db_handler,
                                            buy={"_id":item["_id"]},
                                            value={"status":"CANCEL_ORDERED"})
    def check_buy_completed(self):
        buy_completed = self.db_handler.find_item({"coin":self.coin_type, "status":"BUY_COMPLETED"}, "trader", "trade_status")
        logger.info("BUY_COMPLETED") 
        for item in buy_completed:
            logger.info(item)
            try:
                self.order_sell_transaction(machine=self.machine, db_handler=self.db_handler, coin_type=self.coin_type, item=item)
                #self.pusher.send_message("#push", "sell_ordered:"+str(item))
            except:
                error = traceback.format_exc()
                logger.info(error)
                self.update_trade_status(db_handler=self.db_handler,
                                        buy={"_id":item["_id"]},
                                        value={"error":"failed"})

    def check_sell_ordered(self):
        #Check sell ordered
        sell_orders = self.db_handler.find_item({"coin":self.coin_type,"status":"SELL_ORDERED"}, "trader", "trade_status")
        for item in sell_orders:
            logger.info(item)
            if "sell_order_id" in item:
                order_result = self.machine.get_my_order_status(self.coin_type, item["sell_order_id"])
                if order_result is not None:
                    logger.info(order_result) 
                else:
                    continue
            if len(order_result) > 0 and order_result[0]["status"]=="filled" and order_result[0]["price"] == str(item["desired_value"]):
                order_result_dict = order_result[0]
                real_sell_amount = float(order_result_dict["filled_amount"])
                real_sell_value = float(order_result_dict["avg_price"])
                completed_time = int(order_result_dict["last_filled_at"]/1000)
                fee = float(order_result_dict["fee"])
                if order_result_dict["side"] == "ask":
                    self.update_trade_status(db_handler=self.db_handler,
                                             buy={"_id":item["_id"]},
                                             value={"status":"SELL_COMPLETED",
                                             "real_sell_amount":real_sell_amount,
                                             "sell_completed_time":completed_time,
                                             "real_sell_value":real_sell_value,
                                             "sell_fee":fee})
                    self.pusher.send_message("#push", "sell_completed:"+str(item))
            elif len(order_result) > 0 and order_result[0]["status"] =="unfilled" and order_result[0]["price"] == str(item["desired_value"]):
                if int(item["desired_value"]) > self.last_val*1.15:
                    print(item)
                    self.order_cancel_transaction(machine=self.machine, db_handler=self.db_handler, coin_type=self.coin_type, item=item)
                    #item["desired_value"] = self.last_val
                    #self.order_sell_transaction(machine=self.machine, db_handler=self.db_handler, coin_type=self.coin_type, item=item, order_type="limit")
                    self.update_trade_status(db_handler=self.db_handler,
                                             buy={"_id":item["_id"]},
                                             value={"status":"KEEP_ORDERED"})
                    #self.pusher.send_message("#push", "keeped:"+str(item))

    def check_sell_completed(self):
        sell_completed = self.db_handler.find_item({"coin":self.coin_type, "$or":[{"status":"SELL_COMPLETED"},{"status":"CANCEL_ORDERED"}]}, "trader", "trade_status")
        for item in sell_completed:
            self.db_handler.insert_item(item, "trader", "trade_history")
            self.db_handler.delete_item({"_id":item["_id"]}, "trader", "trade_status")

    def check_keep_ordered(self):
        keeped_orders = self.db_handler.find_item({"coin":self.coin_type, "status":"KEEP_ORDERED"}, "trader", "trade_status")
        for item in keeped_orders:
            if int(item["desired_value"])*0.9 < self.last_val:
                self.order_sell_transaction(machine=self.machine, db_handler=self.db_handler, coin_type=self.coin_type, item=item)
                #self.pusher.send_message("#push", "keep_sell_ordered:"+str(item))
                logger.info("sell order from keeped"+str(item["_id"]))
 
    def check_my_order(self):
        self.check_buy_ordered()
        self.check_buy_completed()
        self.check_sell_ordered()
        self.check_sell_completed()
        self.check_keep_ordered()

    def run(self):
        if self.params["is_active"]=="active":
            self.check_my_order()
            self.scenario()
        else:
            logger.info("inactive")

if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read('conf/config.ini')
    client_id = config['KORBIT']['client_id']
    client_secret = config['KORBIT']['client_secret']
    username = config['KORBIT']['username']
    password = config['KORBIT']['password']
    mongodb = MongoDbHandler("remote", "coiner", "price_info")

    korbit_machine = KorbitMachine(client_id=client_id,
                                   client_secret=client_secret,
                                   username=username,
                                   password=password)
    pusher = PushSlack()
    if len(sys.argv) > 0:
        trader = StepTrade(machine=korbit_machine, db_handler=mongodb, strategy=sys.argv[1], coin_type=sys.argv[2], pusher=pusher)
        trader.run()
    
