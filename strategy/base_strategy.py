from abc import ABC, abstractmethod
import datetime
from logger import get_logger

logger = get_logger("base_strategy")

class Strategy(ABC):
    @abstractmethod
    def run(self):
        pass

    def update_trade_status(self, db_handler=None, buy=None, value=None):
        if db_handler is None or buy is None or value is None:
            raise Exception("Need to buy value or status")
        db_handler.set_db("trader","trade_status")
        db_handler.update_item(buy, {"$set":value})

    def order_buy_transaction(self, machine=None, db_handler=None, coin_type=None, item=None, order_type="limit"):
        if coin_type is None or item is None:
            raise Exception("Need to param")
        db_handler.set_db("trader","trade_status")
        result = machine.buy_order(coin_type=coin_type,
                                    price=str(item["buy"]),
                                    qty=str(item["buy_amount"]),#str(self.BUY_COUNT),
                                    order_type=order_type)
        if result["status"] == "success":
            db_handler.insert_item({"status":"BUY_ORDERED",
                                    "coin":coin_type,
                                    "buy_order_id":str(result["orderId"]),
                                    "buy_amount":float(item["buy_amount"]),
                                    "buy":int(item["buy"]),
                                    "buy_order_time":int(datetime.datetime.now().timestamp()),
                                    "desired_value":int(item["desired_value"]),
                                    "transaction_status":"success",
                                    "machine":str(machine)})
            return result["orderId"]
        else:
            logger.info(result)
            logger.info(item)
            db_handler.update_item({"_id":item["_id"]},{"error": "failed"})
            return None

    def order_sell_transaction(self, machine=None, db_handler=None, coin_type=None, item=None, order_type="limit"):
        if coin_type is None or item is None:
            raise Exception("Need to param")
        db_handler.set_db("trader","trade_status")
        result = machine.sell_order(coin_type=coin_type,
                                        price=str(item["desired_value"]),
                                        qty=str(round(item["real_buy_amount"],8)),
                                        order_type=order_type)
        if result["status"] == "success":
            db_handler.update_item({"_id":item["_id"]},
                        {"$set":{"status":"SELL_ORDERED",
                        "desired_value":int(item["desired_value"]),
                        "sell_order_id":str(result["orderId"]),
                        "error":"success"}
                        })
            return result["orderId"]
        else:
            logger.info(result)
            logger.info(item)
            db_handler.update_item({"_id":item["_id"]},{"error": "failed"})
            return None

    def order_cancel_transaction(self, machine=None, db_handler=None, coin_type=None, item=None):
        db_handler.set_db("trader","trade_status")
        if coin_type is None or item is None:
            raise Exception("Need to param")
        if item["status"] == "BUY_ORDERED":
            result = machine.cancel_order(coin_type=coin_type, order_id=item["buy_order_id"])
            if result[0]["status"] == "success":
                db_handler.update_item({"_id":item["_id"]}, {"$set":{"status":"CANCEL_ORDERED", "cancel_order_time":int(datetime.datetime.now().timestamp()),
                                "error":"success"}})
                return item["buy_order_id"] 
            else:
                logger.info(result)
                logger.info(item)
                return None
        elif item["status"] == "SELL_ORDERED":
            result = machine.cancel_order(coin_type=coin_type, order_id=item["sell_order_id"])
            if result[0]["status"] == "success":
                db_handler.update_item({"_id":item["_id"]}, {"$set":{"status":"CANCEL_ORDERED", "cancel_order_time":int(datetime.datetime.now().timestamp()),
                                "error":"success"}})
                return item["sell_order_id"] 
            else:
                logger.info(result)
                logger.info(item)
                return None
